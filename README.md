# What is protobuf?

Protobuf (short for "Protocol Buffers"), allows communicating data
between various programming languages.
It provides methods to serialize structured data into a binary stream
and deserialize binary streams into structured data.


# Why protobuf?

Common alternatives such as XML and JSON (or derivatives like Hjson),
use significantly more storage (since they store keys as human-readable strings)
and are slower to serialize/deserialize. This makes protobuf more scalable.

Protobuf also has other advantages such as being designed
to support changes in the structure of the structured data.


# Why not protobuf?

Protobuf is less frequently seen in small- and medium-sized projects.
Serialized data is also not immediately human-readable.


# File listing

- `protos/demo.proto`
    - Proto file compiled and used by all languages
    - Handcrafted to contain many common syntax usages
        - Explicitly sets proto version
        - Imports external file
        - Sets option for generated Go package location
        - Uses standard `message` with various types
          (`uint32`, `string`, `enum`, `int32`,
          and well-known types (`google.protobuf.Timestamp`))
        - Shows message nesting
        - Uses `singular` (implicit) and `repeated` field types
        - Single- and multi-line comments
- `src/main.{cpp,go,py}`
    - Runs the following logic:
        1. Read existing protobuf message from file (if it exists),
           otherwise, generate a new protobuf message
        1. Update message to include new history entry
        1. Write updated message back to file
           in order to be read in another language
        1. Write updated message in JSON for comparison
- `.gitlab-ci.yml`
    - Shows the exact Docker environment for compiling and running


# Sample run

```console
# Set alias to show size and contents of serialized data
> alias show_message="gzip --best --keep --force proto-bin/message.json; \
  ls -l proto-bin/message.{bin,json{,.gz}}; \
  protoc --decode=demo.Message proto/demo.proto < proto-bin/message.bin; \
  cat proto-bin/message.json; printf '\n';"


# Note that the full serialized data is only 45 bytes,
# whereas the equivalent in JSON (shown below) is 136 bytes
# To keep the comparison fair, the JSON file was gzipped,
# but the file is still larger (156 bytes)
> ./main-cpp
> show_message
-rw-r--r-- 1 user user  45 Jan 1 00:00 proto-bin/message.bin
-rw-r--r-- 1 user user 136 Jan 1 00:00 proto-bin/message.json
-rw-r--r-- 1 user user 156 Jan 1 00:00 proto-bin/message.json.gz

id: 1
description: "Demo message created in C++"
history {
  value: 1
  author: AUTHOR_CPP
  creation_time {
    seconds: 1672560000
    nanos: 0
  }
}

{"id":1,"description":"Demo message created in C++","history":[{"value":1,"author":"AUTHOR_CPP","creationTime":"2023-01-01T00:00:00Z"}]}


# Serialized data written in one language can be read by another
> ./main-go
> show_message
-rw-r--r-- 1 user user  65 Jan 1 00:00 proto-bin/message.bin
-rw-r--r-- 1 user user 217 Jan 1 00:00 proto-bin/message.json
-rw-r--r-- 1 user user 178 Jan 1 00:00 proto-bin/message.json.gz

id: 1
description: "Demo message created in C++"
history {
  value: 1
  author: AUTHOR_CPP
  creation_time {
    seconds: 1672560000
    nanos: 0
  }
}
history {
  value: 2
  author: AUTHOR_GO
  creation_time {
    seconds: 1672560000
    nanos: 8000
  }
}

{"id":1,"description":"Demo message created in C++","history":[{"value":1,"author":"AUTHOR_CPP","creationTime":"2023-01-01T00:00:00Z"},{"value":2,"author":"AUTHOR_GO","creationTime":"2023-01-01T00:00:00.000008000Z"}]}


> ./main.py
> show_message
-rw-r--r-- 1 user user  85 Jan 1 00:00 proto-bin/message.bin
-rw-r--r-- 1 user user 299 Jan 1 00:00 proto-bin/message.json
-rw-r--r-- 1 user user 195 Jan 1 00:00 proto-bin/message.json.gz

id: 1
description: "Demo message created in C++"
history {
  value: 1
  author: AUTHOR_CPP
  creation_time {
    seconds: 1672560000
    nanos: 0
  }
}
history {
  value: 2
  author: AUTHOR_GO
  creation_time {
    seconds: 1672560000
    nanos: 8000
  }
}
history {
  value: 3
  author: AUTHOR_PYTHON
  creation_time {
    seconds: 1672560000
    nanos: 16000
  }
}

{"id":1,"description":"Demo message created in C++","history":[{"value":1,"author":"AUTHOR_CPP","creationTime":"2023-01-01T00:00:00Z"},{"value":2,"author":"AUTHOR_GO","creationTime":"2023-01-01T00:00:00.000008000Z"},{"value":3,"author":"AUTHOR_PYTHON","creationTime":"2023-01-01T00:00:00.016000Z"}]}
```
