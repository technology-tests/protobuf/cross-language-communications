package main

import (
	"log"
	"os"

	"example.com/protos/demo"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func getOrGenerateMessage(filename string) *demo.Message {
	message := &demo.Message{}

	input, err := os.ReadFile(filename)
	if err == nil {
		if err := proto.Unmarshal(input, message); err != nil {
			log.Fatal(err)
		}

		return message
	}

	message.Id = 2
	message.Description = "Demo message created in Go"

	return message
}

func updateMessage(message *demo.Message) {
	historyItem := &demo.Message_HistoryItem{
		Value:        int32(len(message.History) + 1),
		Author:       demo.Message_HistoryItem_AUTHOR_GO,
		CreationTime: timestamppb.Now(),
	}

	message.History = append(message.History, historyItem)
}

func writeMessage(message *demo.Message, filename string) {
	out, err := proto.Marshal(message)
	if err != nil {
		log.Fatal(err)
	}

	if err := os.WriteFile(filename, out, 0644); err != nil {
		log.Fatal(err)
	}
}

func writeJsonMessage(message *demo.Message, filename string) {
	jsonString, err := protojson.Marshal(message)
	if err != nil {
		log.Fatal(err)
	}

	if err := os.WriteFile(filename, jsonString, 0644); err != nil {
		log.Fatal(err)
	}
}

func main() {
	filename := "proto-bin/message.bin"
	jsonFilename := "proto-bin/message.json"

	message := getOrGenerateMessage(filename)
	updateMessage(message)
	writeMessage(message, filename)

	writeJsonMessage(message, jsonFilename)
}
