#!/usr/bin/env python3

from google.protobuf.json_format import MessageToJson

import importlib
import json

demo_pb2 = importlib.import_module("proto-bin.demo_pb2")


def get_or_generate_message(filename):
    message = demo_pb2.Message()

    try:
        with open(filename, "rb") as f:
            message.ParseFromString(f.read())
    except FileNotFoundError:
        message.id = 3
        message.description = "Demo message created in Python"

    return message


def update_message(message):
    history_item = message.history.add()

    history_item.value = len(message.history)
    history_item.author = demo_pb2.Message.HistoryItem.Author.AUTHOR_PYTHON
    history_item.creation_time.GetCurrentTime()


def write_message(message, filename):
    with open(filename, "wb") as f:
        f.write(message.SerializeToString())


def write_json_message(message, filename):
    # See https://github.com/protocolbuffers/protobuf/issues/9659
    # The JSON serializer for protobuf doesn't support the
    # "minimal spaces" option in Python
    # To work around this, we dump to string, re-parse it,
    # and then dump to string again, but with the Python JSON library
    json_string = MessageToJson(message)

    message_dict = json.loads(json_string)

    with open(filename, "w") as f:
        json.dump(message_dict, f, separators=(",", ":"))


def main():
    filename = "proto-bin/message.bin"
    json_filename = "proto-bin/message.json"

    message = get_or_generate_message(filename)
    update_message(message)
    write_message(message, filename)

    write_json_message(message, json_filename)


if __name__ == "__main__":
    main()
