#include <chrono>
#include <ctime>
#include <fstream>
#include <ios>
#include <iostream>
#include <string>

#include <google/protobuf/timestamp.pb.h>
#include <google/protobuf/util/json_util.h>
#include <google/protobuf/util/time_util.h>

#include "../proto-bin/demo.pb.h"

demo::Message get_or_generate_message(const std::string &filename) {
  std::ifstream input(filename, std::ios::in | std::ios::binary);

  demo::Message message;

  if (input && message.ParseFromIstream(&input)) {
    return message;
  }

  message.set_id(1);
  message.set_description("Demo message created in C++");

  return message;
}

void update_message(demo::Message &message) {
  demo::Message::HistoryItem *history_item = message.add_history();

  history_item->set_value(message.history_size());
  history_item->set_author(demo::Message::HistoryItem::AUTHOR_CPP);

  // See https://github.com/protocolbuffers/protobuf/issues/8930
  // The C++ version of protobuf used in this demo
  // rounds the time to seconds (`nanos` field is always set to zero)
  const google::protobuf::Timestamp timestamp =
      google::protobuf::util::TimeUtil::GetCurrentTime();
  history_item->mutable_creation_time()->CopyFrom(timestamp);
}

int write_message(const demo::Message &message, const std::string &filename) {
  std::ofstream output(filename,
                       std::ios::out | std::ios::trunc | std::ios::binary);

  if (!message.SerializeToOstream(&output)) {
    std::cerr << "Failed to write proto message to " << filename << "\n";
    return -1;
  }

  return 0;
}

void write_json_message(const demo::Message &message,
                        const std::string &filename) {
  std::ofstream output(filename, std::ios::out | std::ios::trunc);

  std::string json_string;

  google::protobuf::util::JsonPrintOptions json_options;
  json_options.add_whitespace = false;
  google::protobuf::util::MessageToJsonString(message, &json_string,
                                              json_options);

  output << json_string;
}

int main() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  const std::string filename = "proto-bin/message.bin";
  const std::string json_filename = "proto-bin/message.json";

  demo::Message message = get_or_generate_message(filename);
  update_message(message);
  int ret_val = write_message(message, filename);

  write_json_message(message, json_filename);

  return ret_val;
}
